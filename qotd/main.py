import uvicorn
from starlette.applications import Starlette
from starlette.responses import PlainTextResponse
from starlette.routing import Route
from random import choice


def get_quote():
    return choice([
        "Carry out a random act of kindness, with no expectation of reward, safe in the knowledge that one day someone might do the same for you.",
        "Family is the most important thing in the world.",
        "The only rock I know that stays steady, the only institution I know that works, is the family.",
        "You are the bows from which your children as living arrows are sent forth.",
        "Our deeds determine us, as much as we determine our deeds.",
        "One must be a living man and a posthumous artist.",
        "Nobody ever forgets where he buried the hatchet."
    ])


def homepage(request):
    return PlainTextResponse(get_quote())


def startup():
    print('Ready to go')


routes = [
    Route('/', homepage),
]

app = Starlette(debug=True, routes=routes, on_startup=[startup])

if __name__ == '__main__':
    uvicorn.run("main:app", host="0.0.0.0", port=5000, log_level="info")
