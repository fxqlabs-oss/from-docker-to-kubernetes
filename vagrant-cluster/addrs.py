import psutil

for key, value in psutil.net_if_addrs().items():
    print("%s - %s" % (key, value[0].address))
