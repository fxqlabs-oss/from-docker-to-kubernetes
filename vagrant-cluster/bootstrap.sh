#! /bin/bash

# Copy Kubernetes Repo Config
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Run General System Update
yum update -y

# Install CURL
yum -y install curl patch diff

# Patch SSHd to enable root ssh access remotely
patch -b /etc/ssh/sshd_config -i /tmp/sshd.patch
service sshd reload

# Add /usr/local/bin to path enabling resolution of locally installed binaries
sudo echo 'export PATH=$PATH:/usr/local/bin' >> /root/.bash_profile
sudo echo 'export PATH=$PATH:/usr/local/bin' >> /root/.bashrc

#Install Python 3.6
sudo yum install -y epel-release gcc
sudo yum install -y python36 python36-pip python36-devel

# Install NeoFetch to print OS info
sudo yum -y install dnf
sudo dnf -y install dnf-plugins-core
sudo dnf -y copr enable konimex/neofetch
sudo dnf -y install neofetch
echo neofetch >> /home/vagrant/.bash_profile
echo neofetch >> /root/.bash_profile

#### Docker, K8s Installation

#Install Docker
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io
systemctl start docker
systemctl enable docker

# Install k8s command line apps
sudo yum install -y kubelet kubeadm kubectl
systemctl enable kubelet
systemctl start kubelet

# Setup interhost communication
sudo echo '172.28.128.5 k8smaster.dmz.localdomain k8smaster' >> /etc/hosts
sudo echo '172.28.128.6 k8sworker1.dmz.localdomain k8sworker1' >> /etc/hosts
sudo echo '172.28.128.7 k8sworker2.dmz.localdomain k8sworker2' >> /etc/hosts

# Add necessary K8s Ports
sudo firewall-cmd --permanent --add-port=6443/tcp # Slave
sudo firewall-cmd --permanent --add-port=2379-2380/tcp # Slave
sudo firewall-cmd --permanent --add-port=10250/tcp # Slave
sudo firewall-cmd --permanent --add-port=10251/tcp # Slave & Master
sudo firewall-cmd --permanent --add-port=10252/tcp # Slave
sudo firewall-cmd --permanent --add-port=10255/tcp # Slave & Master
sudo firewall-cmd --reload

# Set the net.bridge.bridge-nf-call-iptables to ‘1’ in your sysctl config file. 
# This ensures that packets are properly processed by IP tables during filtering and port forwarding.
cat <<EOF > /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system

# Disable SELinux
sudo setenforce 0
sudo sed -i ‘s/^SELINUX=enforcing$/SELINUX=permissive/’ /etc/selinux/config

# Disable SWAP
sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a

# Print out the Network addresses
sudo pip3 install psutil && python3 /tmp/addrs.py
