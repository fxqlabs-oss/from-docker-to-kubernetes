# Pull necessary Kubeadm images
kubeadm config images pull

# Init the Cluster
sudo kubeadm init --apiserver-advertise-address=172.28.128.5 --pod-network-cidr=10.244.0.0/16

# Set kubectl context
sudo echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> /root/.bash_profile
sudo echo 'export KUBECONFIG=/etc/kubernetes/admin.conf' >> /root/.bashrc

# Install Pod network add-on
kubectl apply -f /tmp/kube-flannel.yml

# Install Helm
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash