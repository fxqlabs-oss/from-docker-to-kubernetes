# k8s-flux-gitops
Learning Repo for K8s and Flux Gitops

# Getting Started
To start we need our QOTD application built as a docker image and we should test it using `docker-compose`

## Build Docker Images
First open the qotd folder and run the build script,
This will build and image and tag it with `jonathanturnock/qotd:snapshot`

ℹ It will not be pushed to any remote repositories

```zsh
➜  ~ chmod +x ./build-image.sh   
➜  ~ ./build-image.sh 
```

## Test using Docker-Compose
Now that we have our image built we can run it on our local docker environment using `docker-compose` as a basic sandbox.

```zsh
➜  ~ docker-compose up -d
```

Visit http://localhost:5000 and see that the application is running, stop it with `docker-compose stop`

# Running in K8s
Next up is running the application in K8s, enable K8s in docker desktop to get started

ℹ️ Use Lens to preview the K8s cluster https://k8slens.dev/

![](.media/enable-k8s-docker-desktop.png)

## Deploying to Local K8s 
Located inside the folder `k8s` is 3 raw K8s yml files.

Firstly there is a `pod.yml`, this will deploy a basic pod, we wont be able to interact with this pod at this 
point as its not exposed on the system, but we can create it and see it using the `kubectl get all` command.

Deploy it with
```shell script
$ kubectl apply -f pod.yml
```

Secondly we can create a deployment, this will allow us to have a finer level of control over the pod, for example
we can configure replica sets to ensure there are always 3 pods.

Deploy it with
```shell script
$ kubectl apply -f deployment.yml
```

Finally we need to create the complete service, when we deploy the service it will create a binding which binds the
pods port to a given port on the host its deployed to in the high range. 

Following the service deployment we can reach a pod running the app by visiting http://localhost:30500/

ℹ️ Note whats happening here
1. The Deployment manages the pods, it has provisioned 3 pods, each pod contains an instance of the application.
2. The Service has been exposed on port 30500, when hitting 30500 we hit the service.
3. The Service has a Selector, this Selector is used to find pods and route traffic to an available pod, providing fault tolerance.

```shell script
$ kubectl apply -f service.yml
```

# Setting up K8s Clusters in Virtualbox

Remember to install vbguest tools
```shell script
$ vagrant plugin install vagrant-vbguest
```